
$(function () {
	$('#mailAddother').on('click', function () {
		if ($(this).prop('checked')) {
			$('.mail-address').show();
		} else {
			$('.mail-address').hide();
		}
	});
});

$(function () {
	$('#trustedPerson').on('click', function () {
		if ($(this).prop('checked')) {
			$('.trusted_person').show();
		} else {
			$('.trusted_person').hide();
		}
	});
});

(function (document) {
	'use strict';

	var LightTableFilter = (function (Arr) {
		var _input;

		function _onInputEvent(e) {
			_input = e.target;
			var tables = document.getElementsByClassName(
				_input.getAttribute('data-table')
			);
			Arr.forEach.call(tables, function (table) {
				Arr.forEach.call(table.tBodies, function (tbody) {
					Arr.forEach.call(tbody.rows, _filter);
				});
			});
		}

		function _filter(row) {
			var text = row.textContent.toLowerCase(),
				val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		}

		return {
			init: function () {
				var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function (input) {
					input.oninput = _onInputEvent;
				});
			},
		};
	})(Array.prototype);

	document.addEventListener('readystatechange', function () {
		if (document.readyState === 'complete') {
			LightTableFilter.init();
		}
	});
})(document);

$(document).ready(function () {
	// Add minus icon for collapse element which is open by default
	$('.collapse.show').each(function () {
		$(this)
			.prev('.card-header')
			.find('.fa')
			.addClass('fa-minus')
			.removeClass('fa-plus');
	});

	// Toggle plus minus icon on show hide of collapse element
	$('.collapse')
		.on('show.bs.collapse', function () {
			$(this)
				.prev('.card-header')
				.find('.fa')
				.removeClass('fa-plus')
				.addClass('fa-minus');
		})
		.on('hide.bs.collapse', function () {
			$(this)
				.prev('.card-header')
				.find('.fa')
				.removeClass('fa-minus')
				.addClass('fa-plus');
		});

	if ($(window).width() <= 575) {
		$('.dblogo,.dbnav,.dbadmin').wrapAll(
			'<div id="mobileNav" class="sidenav"></div>'
		);
		$('.sidenav').append(
			"<a href='javascript:void(0)' class='closebtn' onclick='closeNav()'>&times;</a>"
		);
	} else {
		$('.dblogo,.dbnav,.dbadmin').unwrap(
			'<div id="mobileNav" class="sidenav"></div>'
		);
	}
});

// $('.next').click(function(){
//    $(this).parent().hide().next().show();//hide parent and show next
// });

// $('.next').click(function(){
//      $(this).parent().hide().next().show();
// });
var left, opacity, scale; //fieldset properties which we will animate
var animating;

$('.stepbox-1').submit(function (ev) {
	alert('asd');
	ev.preventDefault();
	var form = $('.stepbox-1');
	if (form.valid() === false) {
		ev.stopPropagation();
	}
});

function stepBox1() {
	var form = $('.stepbox-1');
	var validator = form.data('validator');
	var fields = form.find('.validate');

	if (form.valid()) {
		var formSerialize = form.serialize();
		$.ajax({
			url: '/register-detail1',
			method: 'post',
			dataType: 'json',
			data: formSerialize,
			beforeSend: function () {
				$('#stepbox1-btn').prop('disabled', true);
			},
			success: function (response, xhr) {
				if (response.success == 1) {
					$('.stepbox-2').show();
					$('.stepbox-1').hide();
					$('#progressbar li')
						.eq($('form').index($('.stepbox-2')))
						.addClass('active');
					$('#stepbox1-btn').prop('disabled', false);
					$('.container, html').animate({
							scrollTop: $('.container').offset().top
						},
						'slow'
					);

					//   if(animating) return false;
					//   animating = true;
					//   form.animate({opacity: 0}, {
					//     step: function(now, mx) {
					//         //as the opacity of current_fs reduces to 0 - stored in "now"
					//         //1. scale current_fs down to 80%
					//         scale = 1 - (1 - now) * 0.2;
					//         //2. bring next_fs from the right(50%)
					//         left = (now * 50)+"%";
					//         //3. increase opacity of next_fs to 1 as it moves in
					//         opacity = 1 - now;
					//         form.css({
					//     'transform': 'scale('+scale+')',
					//     'position': 'relative',
					//     'top'     : '0'
					//   });
					//   $('.stepbox-2').css({'left': left, 'opacity': opacity});
					//     },
					//     duration: 500,
					//     complete: function(){
					//         form.hide();
					//         animating = false;
					//     },
					//     //this comes from the custom easing plugin
					//     easing: 'easeInQuad'
					// });
				}
			},
			error: function (response, xhr, error) {
				location.reload(true);
			},
		});
		//}
	} else {
		$('[data-toggle="tooltip"]').tooltip('open');
	}
	form.addClass('was-validated');
}

function stepBox2() {
	var form = $('.stepbox-2');
	var validator = form.data('validator');
	var fields = form.find('.validate');

	if (fields.valid()) {
		var formSerialize = form.serialize();
		$.ajax({
			url: '/register-detail2',
			method: 'post',
			dataType: 'json',
			data: formSerialize,
			beforeSend: function () {
				$('#stepbox2-btn').prop('disabled', true);
			},
			success: function (response) {
				if (response.success == 1) {
					$('.stepbox-3').show();
					$('.stepbox-2').hide();
					$('#progressbar li')
						.eq($('form').index($('.stepbox-3')))
						.addClass('active');
					$('#stepbox2-btn').prop('disabled', false);
					$('.container, html').animate({
							scrollTop: $('.container').offset().top
						},
						'slow'
					);
				}
			},
			error: function (response, xhr, error) {
				location.reload(true);
			},
		});
	}
	form.addClass('was-validated');
}

function stepBox3() {
	var form = $('.stepbox-3');
	var validator = form.data('validator');
	var fields = form.find('.validate');

	if (fields.valid()) {
		var formSerialize = form.serialize();
		$.ajax({
			url: '/register-detail3',
			method: 'post',
			dataType: 'json',
			data: formSerialize,
			beforeSend: function () {
				$('#stepbox3-btn').prop('disabled', true);
			},
			success: function (response) {
				if (response.success == 1) {
					$('.stepbox-4').show();
					$('.stepbox-3').hide();
					$('#progressbar li')
						.eq($('form').index($('.stepbox-4')))
						.addClass('active');
					$('#stepbox3-btn').prop('disabled', false);
					$('.container, html').animate({
							scrollTop: $('.container').offset().top
						},
						'slow'
					);
				}
			},
			error: function (response, xhr, error) {
				location.reload(true);
			},
		});
	}
	form.addClass('was-validated');
}

function stepBox4() {
	var form = $('.stepbox-4');

	let flag = 0;
	if (
		$('#annualIncome:checked').val() == undefined ||
		$('#annualIncome:checked').val() == '$25,000 and under' ||
		$('#liquidNetWorth:checked').val() == undefined ||
		$('#liquidNetWorth:checked').val() == '$25,000 and Under' ||
		$('#netWorth:checked').val() == undefined ||
		$('#netWorth:checked').val() == '$25,000 and Under' ||
		$('#taxRate:checked').val() == undefined ||
		$('#liquidNetWorth:checked').data('value') >
		$('#netWorth:checked').data('value')
	) {
		flag = 0;
	} else {
		$('.annualIncome-error').html('');
		$('.liquidNetWorth-error').html('');
		$('.label-icon')
			.find('liquidNetWorth_error')
			.removeClass('liquidNetWorth_error');
		$('.netWorth-error').html('');
		$('.taxRate-error').html('');

		flag = 1;
	}

	if (flag == 1) {
		var formSerialize = form.serialize();
		$.ajax({
			url: '/register-detail4',
			method: 'post',
			dataType: 'json',
			data: formSerialize,
			beforeSend: function () {
				$('#stepbox4-btn').prop('disabled', true);
			},
			success: function (response) {
				if (response.success == 1) {
					$('.stepbox-5').show();
					$('.stepbox-4').hide();
					$('#progressbar li')
						.eq($('form').index($('.stepbox-5')))
						.addClass('active');
					$('#stepbox4-btn').prop('disabled', false);
					$('.container, html').animate({
							scrollTop: $('.container').offset().top
						},
						'slow'
					);
				}
			},
			error: function (response, xhr, error) {
				location.reload(true);
			},
		});
	} else {
		if ($('#annualIncome:checked').val() == undefined) {
			$('.annualIncome-error').html(
				'<span style="color:red"> Annual Income is required </span>'
			);
		} else if ($('#annualIncome:checked').val() == '$25,000 and under') {
			$('.annualIncome-error').html(
				'<span style="color:red"> The Annual Income selection you have chosen is inconsistent with Annual Income of a Pattern Day Trader. Please make another selection ONLY IF you have chosen this in error. If you have NOT made this selection in error, we are unable to open this account at this time. Thank you. </span>'
			);
		} else {
			$('.annualIncome-error').html('');
		}
		if ($('#liquidNetWorth:checked').val() == undefined) {
			$('.liquidNetWorth-error').html(
				'<span style="color:red"> Liquid net worth is required </span>'
			);
		} else if ($('#liquidNetWorth:checked').val() == '$25,000 and Under') {
			$('.liquidNetWorth-error').html(
				'<span style="color:red"> The Net Worth selection you have chosen is inconsistent with the Net Worth of a Pattern Day Trader. Please make another selection ONLY IF you have chosen this in error. If you have NOT made this selection in error, we are unable to open this account at this time. Thank you. </span>'
			);
		} else {
			$('.liquidNetWorth-error').html('');
		}
		if ($('#netWorth:checked').val() == undefined) {
			$('.netWorth-error').html(
				'<span style="color:red"> Net worth is required </span>'
			);
		} else if ($('#netWorth:checked').val() == '$25,000 and Under') {
			$('.netWorth-error').html(
				'<span style="color:red"> The Liquid Net Worth selection you have chosen is inconsistent with the Liquid Net Worth of a Pattern Day Trader. Please make another selection ONLY IF you have chosen this in error. If you have NOT made this selection in error, we are unable to open this account at this time. Thank you. </span>'
			);
		} else {
			$('.netWorth-error').html('');
		}
		if ($('#taxRate:checked').val() == undefined) {
			$('.taxRate-error').html(
				'<span style="color:red"> Net worth is required </span>'
			);
		} else {
			$('.taxRate-error').html('');
		}
		if (
			$('#netWorth:checked').val() !== undefined &&
			$('#liquidNetWorth:checked').val() !== undefined &&
			$('#liquidNetWorth:checked').data('value') >
			$('#netWorth:checked').data('value')
		) {
			$('#liquidNetWorth:checked')
				.parents('label-icon')
				.addClass('liquidNetWorth_error');

			$('.liquidNetWorth-error').html(
				'<span style="color:red"> Liquid net worth should not be greater than net worth </span>'
			);
		} else {
			$('div[style="border: 2px solid red;"]');
		}
	}

	form.addClass('was-validated');
}

function stepBox5() {
	var form = $('.stepbox-5');
	var validator = form.data('validator');
	var fields = form.find('.validate');

	if (fields.valid()) {
		var formSerialize = form.serialize();
		$.ajax({
			url: '/register-detail5',
			method: 'post',
			dataType: 'json',
			data: formSerialize,
			beforeSend: function () {
				$('#stepbox5-btn').prop('disabled', true);
			},
			success: function (response) {
				if (response.success == 1) {
					$('.stepbox-6').show();
					$('.stepbox-5').hide();
					$('#progressbar li')
						.eq($('form').index($('.stepbox-6')))
						.addClass('active');
					$('#stepbox5-btn').prop('disabled', false);
					$('.container, html').animate({
							scrollTop: $('.container').offset().top
						},
						'slow'
					);
				}
			},
			error: function (response, xhr, error) {
				location.reload(true);
			},
		});
	}
	form.addClass('was-validated');
}

function stepBox6() {
	var form = $('.stepbox-6');
	var validator = form.data('validator');
	var fields = form.find('.validate');
	let conservative = 0;
	let moderatelyConservative = 0;
	let moderate = 0;
	let moderatelyAggressive = 0;
	let significantRisk = 0;
	let counttolerance = 0;
	let message;
	let flag = 0;

	if ($('#conservative').is(':checked')) {
		conservative = 1;
		counttolerance++;
	}
	if ($('#moderatelyConservative').is(':checked')) {
		moderatelyConservative = 1;
		counttolerance++;
	}
	if ($('#moderate').is(':checked')) {
		moderate = 1;
		counttolerance++;
	}
	if ($('#moderatelyAggressive').is(':checked')) {
		moderatelyAggressive = 1;
		counttolerance++;
	}

	if ($('#significantRisk').is(':checked')) {
		significantRisk = 1;
		counttolerance++;
	}

	if (counttolerance > 1) {
		message =
			'Multiple Risk Tolerance Options Cannot Be Checked.  Please Select Only 1 Risk Tolerance Option Below.';
	} else if (counttolerance == 0) {
		message = 'Please Select Atleast 1 Risk Tolerance Option Below.';
	} else if (
		$('#conservative').is(':checked') ||
		$('#moderatelyConservative').is(':checked') ||
		$('#moderate').is(':checked') ||
		$('#moderatelyAggressive').is(':checked')
	) {
		message =
			'The Risk Tolerance Option you selected is inconsistent with the Risk Tolerance of a Pattern Day Trader.  Please Select Another Risk Tolerance Option ONLY IF you Selected this Option in Error.  If you DID NOT select this Risk Tolerance Option in error, we are unable to offer you an account at this time.  Thank you.';
	} else {
		flag = 1;
	}
	if (flag) {
		$('.tolerance-error').html('');
		if (fields.valid()) {
			var formSerialize = form.serialize();
			$.ajax({
				url: '/register-detail6',
				method: 'post',
				dataType: 'json',
				data: formSerialize,
				beforeSend: function () {
					$('#stepbox6-btn').prop('disabled', true);
				},
				success: function (response) {
					if (response.success == 1) {
						$('.stepbox-7').show();
						$('.stepbox-6').hide();
						$('#progressbar li')
							.eq($('form').index($('.stepbox-7')))
							.addClass('active');
						$('#stepbox6-btn').prop('disabled', false);
						$('.container, html').animate({
								scrollTop: $('.container').offset().top
							},
							'slow'
						);
					}
				},
				error: function (response, xhr, error) {
					location.reload(true);
				},
			});
		} else {
			if ($('#significantRisk').hasClass('error')) {
				$('label[for="significantRisk"]').addClass('error-new');
			} else {
				$('label[for="significantRisk"]').removeClass('error-new');
			}
		}
	} else {
		$('.tolerance-error').html(
			'<span style="color:red">' + message + '</span>'
		);
	}
	form.addClass('was-validated');
}

function stepBox7() {
	var form = $('.stepbox-7');
	var validator = form.data('validator');
	var fields = form.find('.validate');

	let flag = 0;
	if (
		$('#annualExpense:checked').val() == undefined ||
		$('#annualExpense:checked').data('value') >
		$('#annualIncome:checked').data('value') ||
		$('#specialExpense:checked').val() == undefined ||
		$('#liquidityExpense:checked').val() == undefined ||
		$('#financialGoal:checked').val() == undefined
	) {
		flag = 0;
	} else {
		$('.annualExpense-error').html('');
		$('.specialExpense-error').html('');

		$('.liquidityExpense-error').html('');
		$('.financialGoal-error').html('');

		flag = 1;
	}
	if (flag == 1) {
		var formSerialize = form.serialize();
		$.ajax({
			url: '/register-detail7',
			method: 'post',
			dataType: 'json',
			data: formSerialize,
			beforeSend: function () {
				$('#stepbox7-btn').prop('disabled', true);
			},
			success: function (response) {
				if (response.success == 1) {
					$('.stepbox-8').show();
					$('.stepbox-7').hide();
					$('#progressbar li')
						.eq($('form').index($('.stepbox-8')))
						.addClass('active');
					$('#stepbox7-btn').prop('disabled', false);
					$('.container, html').animate({
							scrollTop: $('.container').offset().top
						},
						'slow'
					);
				}
			},
			error: function (response, xhr, error) {
				location.reload(true);
			},
		});
	} else {
		if ($('#annualExpense:checked').val() == undefined) {
			$('.annualExpense-error').html(
				'<span style="color:red"> Annual Expense is required </span>'
			);
		} else if (
			$('#annualExpense:checked').data('value') >
			$('#annualIncome:checked').data('value')
		) {
			$('.annualExpense-error').html(
				'<span style="color:red"> Your Annual Expenses are Greater than the level of Income you selected in this application.  This is inconsistent with the Risk Tolerance of a Pattern Day Trader.  Please make another selection ONLY IF you selected this in error.  If you did NOT select this option in error, we are unable to open an account for you at this time.  Thank You. </span>'
			);
		} else {
			$('.annualExpense-error').html('');
		}
		if ($('#specialExpense:checked').val() == undefined) {
			$('.specialExpense-error').html(
				'<span style="color:red"> Special Expense worth is required </span>'
			);
		} else {
			$('.specialExpense-error').html('');
		}
		if ($('#liquidityExpense:checked').val() == undefined) {
			$('.liquidityExpense-error').html(
				'<span style="color:red"> Liquidity Expense is required </span>'
			);
		} else {
			$('.liquidityExpense-error').html('');
		}
		if ($('#financialGoal:checked').val() == undefined) {
			$('.financialGoal-error').html(
				'<span style="color:red"> Financial Goal is required </span>'
			);
		} else {
			$('.financialGoal-error').html('');
		}
	}
	form.addClass('was-validated');
}

function stepBox8() {
	var form = $('.stepbox-8');
	var validator = form.data('validator');
	var fields = form.find('.validate');
	let stockExpertiseError = '';

	let flag = 1;
	if (
		$('#investStock:checked').val() == undefined &&
		$('#investBond:checked').val() == undefined &&
		$('#investOptions:checked').val() == undefined &&
		$('#investFuture:checked').val() == undefined
	) {
		stockExpertiseError = 'Investment Experience must be select one.';
		flag = 0;
	} else if ($('#stockExpertise:checked').val() == 'None') {
		stockExpertiseError =
			'The Level of Investment Experience you selected is Inconsistent with the level of Investor Sophistication for a Pattern Day Trader.  Please make another selection ONLY IF you selected this in error.  If you did NOT make this selection in error, we are unable to open an account for you at this time.';
		flag = 0;
	} else if ($('#stockExpertise:checked').val() == 'Limited') {
		stockExpertiseError =
			'The Level of Investment Experience you selected is Inconsistent with the level of Investor Sophistication for a Pattern Day Trader.  Please make another selection ONLY IF you selected this in error.  If you did NOT make this selection in error, we are unable to open an account for you at this time.';
		flag = 0;
	} else {
		$('.stockExpertise-error').html('');
	}
	if (flag) {
		// if (fields.valid()) {
		//  var formSerialize = new FormData(form[0]);

		//var formSerialize = form.serialize();
		// $.ajax({
		//   url: '/register-detail8',
		//   method: 'post',
		//   dataType: 'json',
		//   cache: false,
		//   contentType: false,
		//   processData: false,
		//   data: formSerialize,

		var formSerialize = form.serialize();
		$.ajax({
			url: '/register-detail8',
			method: 'post',
			dataType: 'json',
			data: formSerialize,

			beforeSend: function () {
				$('#stepbox8-btn').prop('disabled', true);
			},
			success: function (response) {
				if (response.success == 1) {
					$('.stepbox-9').show();
					$('.stepbox-8').hide();
					$('#progressbar li')
						.eq($('form').index($('.stepbox-9')))
						.addClass('active');
					$('#stepbox8-btn').prop('disabled', false);
					$('.container, html').animate({
							scrollTop: $('.container').offset().top
						},
						'slow'
					);
				}
			},
			error: function (response, xhr, error) {
				location.reload(true);
			},
		});
	} else {
		form.addClass('was-validated');
		// } else {
		$('.stockExpertise-error').html(
			'<span style="color:red"> ' + stockExpertiseError + ' </span>'
		);
	}
	//}
}

function stepBox9() {
	var form 		= $('.stepbox-9');
	var validator 	= form.data('validator');
	var fields 		= form.find('.validate');
	var flag = 1;
	var files = $("input[name='photo[]']")
					.filter(function () {
						return $(this).val() != '';
					})
					.get();

	if (files < 2) {
		flag = 0;
	} else {
		$('.photo-error').html('');
	}

	if (flag) {
		if (fields.valid()) {
			// var photofile = $("input[name='photo[]']")[0].files;
			// var corporate_photofile = $("input[name='corporate_photo[]']")[0].files;
			var formSerialize = new FormData(form[0]);
			// formSerialize.append("photofile", 			photofile);
			// formSerialize.append("corporate_photofile", 	corporate_photofile);
			$.ajax({
				url: '/register-detail9',
				method: 'post',
				data: formSerialize,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function () {
					$('#stepbox9-btn').prop('disabled', true);
				},
				success: function (response) {
					if (response.success == 1) {
						$('.stepbox-10').show();
						$('.stepbox-9').hide();
						$('#progressbar li')
							.eq($('form').index($('.stepbox-10')))
							.addClass('active');
						$('#stepbox9-btn').prop('disabled', false);
						$('.container, html').animate({
								scrollTop: $('.container').offset().top
							},
							'slow'
						);
					}
				},
				error: function (response, xhr, error) {
					console.log({response, xhr, error})
				},
			});
		}
		form.addClass('was-validated');
	} else {
		$('.photo-error').html(
			'<span style="color:red"> Please Upload a minimum 2 </span>'
		);
	}
}

function stepBox10() {
	var form = $('.stepbox-10');
	var validator = form.data('validator');
	var fields = form.find('.validate');
	// let seniorError = '';
	// let houseHoldError = '';
	// let municipalError = '';
	let optRadioError = '';
	let brokerAccountError = '';
	let beneficialError = '';
	let shareHolderError = '';
	let securitiesError = '';
	let immediateError = '';

	let flag = 1;
	if (
		$('#optRadio:checked').val() == undefined ||
		$('#optRadio:checked').val() == '0'
	) {
		optRadioError = 'Required !';
		flag = 0;
	} else {
		$('.optRadio-error').html('');
		flag = 1;
	}
	if ($('#brokerAccount:checked').val() == undefined) {
		brokerAccountError = 'Required !';
		flag = 0;
	} else {
		$('.brokerAccount-error').html('');
	}
	if ($('#beneficial:checked').val() == undefined) {
		beneficialError = 'Required !';
		flag = 0;
	} else {
		$('.beneficial-error').html('');
	}
	if ($('#shareHolder:checked').val() == undefined) {
		shareHolderError = 'Required !';
		flag = 0;
	} else {
		$('.shareHolder-error').html('');
	}
	if ($('#securities:checked').val() == undefined) {
		securitiesError = 'Required !';
		flag = 0;
	} else {
		$('.securities-error').html('');
	}
	if ($('#immediate:checked').val() == undefined) {
		immediateError = 'Required !';
		flag = 0;
	} else {
		$('.immediate-error').html('');
	}
	if ($('#institution:checked').val() == undefined) {
		institutionError = 'Required !';
		flag = 0;
	} else {
		$('.institution-error').html('');
	}

	// if ($('#senior:checked').val() == undefined) {
	//   seniorError = 'Required !';
	//   flag = 0;
	// } else {
	//   $('.senior-error').html('');
	//   flag = 1;
	// }
	// if ($('#houseHold:checked').val() == undefined) {
	//   houseHoldError = 'Required !';
	//   flag = 0;
	// } else {
	//   $('.houseHold-error').html('');
	//   flag = 1;
	// }
	// if ($('#municipal:checked').val() == undefined) {
	//   municipalError = 'Required !';
	//   flag = 0;
	// } else {
	//   $('.municipal-error').html('');
	//   flag = 1;
	// }
	if (flag) {
		if (fields.valid()) {
			var formSerialize = new FormData(form[0]);

			$.ajax({
				url: '/register-detail10',
				method: 'post',
				data: formSerialize,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function () {
					$('#stepbox10-btn').prop('disabled', true);
				},
				success: function (response) {
					if (response.success == 1) {
						$('.stepbox-11').show();
						$('.stepbox-10').hide();
						$('#progressbar li')
							.eq($('form').index($('.stepbox-11')))
							.addClass('active');
						$('#stepbox10-btn').prop('disabled', false);
						$('.container, html').animate({
								scrollTop: $('.container').offset().top
							},
							'slow'
						);
					}
				},
				error: function (response, xhr, error) {
					location.reload(true);
				},
			});
		}
		form.addClass('was-validated');
	} else {
		// $('.senior-error').html(
		//   '<span style="color:red"> ' + seniorError + ' </span>'
		// );
		// $('.houseHold-error').html(
		//   '<span style="color:red"> ' + houseHoldError + ' </span>'
		// );
		// $('.municipal-error').html(
		//   '<span style="color:red"> ' + municipalError + ' </span>'
		// );
		$('.optRadio-error').html(
			'<span style="color:red"> ' + optRadioError + ' </span>'
		);

		$('.brokerAccount-error').html(
			'<span style="color:red"> ' + brokerAccountError + ' </span>'
		);
		$('.beneficial-error').html(
			'<span style="color:red"> ' + beneficialError + ' </span>'
		);
		$('.shareHolder-error').html(
			'<span style="color:red"> ' + shareHolderError + ' </span>'
		);
		$('.securities-error').html(
			'<span style="color:red"> ' + securitiesError + ' </span>'
		);
		$('.immediate-error').html(
			'<span style="color:red"> ' + immediateError + ' </span>'
		);
		$('.institution-error').html(
			'<span style="color:red"> ' + institutionError + ' </span>'
		);
	}
}

function stepBox11() {
	var form = $('.stepbox-11');
	var validator = form.data('validator');
	var fields = form.find('.validate');

	if (fields.valid()) {
		var formSerialize = form.serialize();
		$.ajax({
			url: '/register-detail11',
			method: 'post',
			dataType: 'json',
			data: formSerialize,
			beforeSend: function () {
				$('#stepbox10-btn').prop('disabled', true);
			},
			success: function (response) {
				//window.location.replace('ds/login');
				if (response.success == 1) {
					$('.stepbox-12').show();
					// const binaryString = window.atob(response.pdfBinary); // Comment this if not using base64
					// const bytes = new Uint8Array(binaryString.length);
					// const base64ToArrayBuffer = bytes.map((byte, i) =>
					//   binaryString.charCodeAt(i)
					// );
					// console.log(response.pdfBinary);
					// var blob = new Blob([
					//   'response.pdfBinary',
					//   { type: 'application/pdf' },
					// ]);
					// const fileName = 'application.pdf';
					// if (navigator.msSaveBlob) {
					//   // IE 10+
					//   navigator.msSaveBlob(blob, fileName);
					// } else {
					//   const link = document.createElement('a');
					//   // Browsers that support HTML5 download attribute
					//   if (link.download !== undefined) {
					//     const url = URL.createObjectURL(blob);
					//     link.setAttribute('href', url);
					//     link.setAttribute('download', fileName);
					//     link.style.visibility = 'hidden';
					//     document.body.appendChild(link);
					//     link.click();
					//     document.body.removeChild(link);
					//     setTimeout(function () {
					//       // For Firefox it is necessary to delay revoking the ObjectURL
					//       window.URL.revokeObjectURL(url);
					//     }, 200);
					//   }
					// }
					// window.location.href = response.pdf;
					$('.stepbox-11').hide();
					$('#progressbar li')
						.eq($('form').index($('.stepbox-11')))
						.addClass('active');
					$('#stepbox11-btn').prop('disabled', false);
					$('.container, html').animate({
							scrollTop: $('.container').offset().top
						},
						'slow'
					);
				}
			},
			error: function (response, xhr, error) {
				location.reload(true);
			},
		});
	} else {
		if ($('#accountTerms').hasClass('error')) {
			$('label[for="accountTerms"]').addClass('error-new');
		} else {
			$('label[for="accountTerms"]').removeClass('error-new');
		}

		if ($('#riskDis').hasClass('error')) {
			$('label[for="riskDis"]').addClass('error-new');
		} else {
			$('label[for="riskDis"]').removeClass('error-new');
		}

		if ($('#pennyStocks').hasClass('error')) {
			$('label[for="pennyStocks"]').addClass('error-new');
		} else {
			$('label[for="pennyStocks"]').removeClass('error-new');
		}

		if ($('#electronicAccess').hasClass('error')) {
			$('label[for="electronicAccess"]').addClass('error-new');
		} else {
			$('label[for="electronicAccess"]').removeClass('error-new');
		}

		if ($('#marginDisclosure').hasClass('error')) {
			$('label[for="marginDisclosure"]').addClass('error-new');
		} else {
			$('label[for="marginDisclosure"]').removeClass('error-new');
		}

		if ($('#w9_Certification').hasClass('error')) {
			$('label[for="w9_Certification"]').addClass('error-new');
		} else {
			$('label[for="w9_Certification"]').removeClass('error-new');
		}

		if ($('#stock_Locate').hasClass('error')) {
			$('label[for="stock_Locate"]').addClass('error-new');
		} else {
			$('label[for="stock_Locate"]').removeClass('error-new');
		}

		if ($('#marginDisc').hasClass('error')) {
			$('label[for="marginDisc"]').addClass('error-new');
		} else {
			$('label[for="marginDisc"]').removeClass('error-new');
		}

		if ($('#confirmedElectronic').hasClass('error')) {
			$('label[for="confirmedElectronic"]').addClass('error-new');
		} else {
			$('label[for="confirmedElectronic"]').removeClass('error-new');
		}
	}
	form.addClass('was-validated');
}

$('.previous').click(function () {
	$(this).parent().hide().prev().show(); //hide parent and show previous
	$('#progressbar li')
		.eq($('form').index($(this)))
		.removeClass('active');
});

$(window).resize(function () {
	if ($(window).width() <= 575) {
		$('.dblogo,.dbnav,.dbadmin').wrapAll(
			'<div id="mobileNav" class="sidenav"></div>'
		);
		$('.sidenav').append(
			"<a href='javascript:void(0)' class='closebtn' onclick='closeNav()'>&times;</a>"
		);
	} else {
		$('.dblogo,.dbnav,.dbadmin').unwrap(
			'<div id="mobileNav" class="sidenav"></div>'
		);
	}
});

function openNav() {
	document.getElementById('mobileNav').style.width = '250px';
}

function closeNav() {
	document.getElementById('mobileNav').style.width = '0';
}

$('.label-icon').click(function () {
	$(this).addClass('radiocurrent').siblings().removeClass('radiocurrent');
});

if (/Mobi/.test(navigator.userAgent)) {
	// if mobile device, use native pickers
	$('.date input').attr('type', 'date');
	$('.time input').attr('type', 'time');
} else {
	// if desktop device, use DateTimePicker
}